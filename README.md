# TeensyCS (communication service)

The TeensyCS (communication service) is a Java-library to communicate with a teensy/arduino boards over FTDI-interface (COM-port). The library supports:

- Automatic search and establish a connection with the desired device
- Control data transmition with an Adler-32 checksum
- Data type conversion

## Packet formats

The TeensyCS library uses two types of a packets: for sending a request and getting response from a teensy board.

### Request packet

 Offset | Field name  | Value | Size   | Description
--------|-------------|-------|--------|----------------------------------------------------------
 0x00   | Start frame | 0x55  | 1 byte | Value to indicate start of the request packet
 0x01   | Command     |       | 1 byte | Code of a teensy command
 0x02   | Data length |       | 1 byte | Length of the data
 0x03   | Checksum    |       | 4 byte | CRC32 checksum of the data
 0x07   | Data        |       |  var.  | A data. Size of the data block is defined by "Data length" field (max. 256 bytes)


### Response packet

 Offset | Field name  | Value | Size   | Description
--------|-------------|-------|--------|-------------------------------------------------------
 0x00   | Start frame | 0x55  | 1 byte | Value to indicate start of the response packet
 0x01   | Command     |       | 1 byte | Code of a teensy command, which sends a response
 0x02   | Result      |       | 1 byte | Command result: 0 if success, otherwise error code
 0x03   | Data length |       | 1 byte | Length of the data
 0x04   | Checksum    |       | 4 byte | CRC32 checksum of the data
 0x08   | Data        |       |  var.  | A data. Size of the data block is defined by "Data lenght" field (max. 256 bytes)

### Commands

The teensy service supports automatic connection, connection check and re-connection routines. For using this features the teensy-side must support at least two commands:

 Command code | Command name | Request params | Response                | Description
--------------|--------------|----------------|-------------------------|-------------------------------------------------------------------------
 0xFF         | CMD_GET_ID   | -              | Firmware ID (1 byte)    | Get firmware ID
 0xFE         | CMD_NOP      | -              | -                       | No operation: the command will be used for checking a connection state
 
 ### Examples
 
 #### Java
 
 Hier is an example for using teensy service in Java:
 
```
public class MyClass implements TeensyServiceListener {
	/**
	 * The teensy firmware ID. Will be used for searching desired port.
	 */
	public static final byte FIRMWARE_ID = (byte) 0xAA;
	
	/**
	 *
	 */
	private static final byte CMD_GET_LED_STATE = (byte) 0x01;

	/**
	 * LED index
	 */	
	private static final byte LED0_IDX = 0;
	
	/**
	 * The {@link TeensyService} to use
	 */
	private TeensyService teensyService;
	
	/**
	 * Close application event: stopping teensy service
	 */
	public void closeApp() {
		teensyService.stopService();
	}
	
	/**
	 * Initialize APT library
	 */
	public MyClass() {
		teensyService = TeensyService.buildService(FIRMWARE_ID);
		teensyService.addListener(this);
	}
	
	/**
	 * Connected to the teensy: enable visual controls and request actual LED state
	 */
	public void onTeensyConnected() {
		System.out.println("Connected");
	}

	/**
	 * Disconnected from the teensy: disable visual controls
	 */
	public void onTeensyDisconnected() {
		System.out.println("Disconnected");
	}

	/**
	 * Implementation of {@link TeensyServiceListener#onTeensyConnecting()}
	 */
	public void onTeensyConnecting() {
		System.out.println("Try to connect...");
	}

	/**
	 * Implementation of {@link TeensyServiceListener#onTeensyDisconnecting()}
	 */
	public void onTeensyDisconnecting() {
		System.out.println("Disconnecting...");
	}
	
	...
	
	/**
	 * Example of sending a request and handle response
	 */
	public void sendSomeRequest() {
		// Send command to the teensy and handle request
		TeensyRequest request = new TeensyRequest(CMD_GET_LED_STATE);
		request.writeByte(LED0_IDX);
		
		TeensyResponse resp = teensyService.sendPacket(request);
		
		System.out.println("LED state: " + resp.readByte());
	}
	
}
 ```
 
 #### Teensy

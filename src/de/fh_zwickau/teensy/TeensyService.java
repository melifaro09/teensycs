package de.fh_zwickau.teensy;

import java.util.Timer;
import java.util.TimerTask;
import com.fazecast.jSerialComm.SerialPort;

/**
 * The service to communicate with a teensy board via COM interface
 * 
 * @author Alexandr Mitiaev
 *
 */
public class TeensyService extends GenericNotifyer<TeensyServiceListener> {
	/**
	 * The connection state enum
	 * 
	 * <ul>
	 * <li><code>STATE_CONNECTED</code>		-	Connected to the teensy board</li>
	 * <li><code>STATE_DISCONNECTED</code>	-	No connection to the teensy board</li>
	 * <li><code>STATE_CONNECTING</code>		-	Connection establishing run</li>
	 * <li><code>STATE_DISCONNECTED</code>	-	Connection closing run</li>
	 * </ul>
	 */
	public enum EConnectionState { STATE_CONNECTED, STATE_DISCONNECTED, STATE_CONNECTING, STATE_DISCONNECTING };
	
	/**
	 * The COM port baud rate
	 */
	private static final int BAUD_RATE						=				115200;

	/**
	 * Repeat connection time (default 10 sec.)
	 */
	private static final int CONNECT_REPEAT_TIME			=				5000;
	
	/**
	 * State of the connection
	 */
	private EConnectionState connectionState				=				EConnectionState.STATE_DISCONNECTED;
	
	/**
	 * The opened {@link SerialPort}
	 */
	private SerialPort serialPort							=				null;
	
	/**
	 * The connection repeat timer
	 */
	private Timer connectionTimer							=				null;
	
	/**
	 * The firmware ID
	 */
	private int firmwareId;
	
	/**
	 * Private constructor
	 */
	private TeensyService() {
	}
	
	/**
	 * Build the new instance of {@link TeensyService} to communicate with a teensy board
	 * 
	 * @param firmware			The firmware ID number
	 * @param label				The label to display connection status. May be <b>null</b>.
	 * @param shape				The shape to display connection status. May be <b>null</b>.
	 * 
	 * @return	The {@link TeensyService} instance
	 */
	public static TeensyService buildService(final int firmware) {
		final TeensyService instance = new TeensyService();
		instance.startService(firmware);

		return instance;
	}
	
	/**
	 * Return true, if connected to the teensy, otherwise false
	 * 
	 * @return True if connected, otherwise false
	 */
	public synchronized boolean isConnected() {
		return connectionState == EConnectionState.STATE_CONNECTED;
	}
	
	/**
	 * Returns true, if disconnected from teensy, otherwise false
	 * 
	 * @return True if disconnected
	 */
	public synchronized boolean isDisconnected() {
		return connectionState == EConnectionState.STATE_DISCONNECTED;
	}
	
	/**
	 * Returns the connection status as a {@link EConnectionState} enum
	 * 
	 * @return The {@link EConnectionState} with a current connection state
	 */
	public synchronized EConnectionState getState() {
		return connectionState;
	}
	
	/**
	 * Initialize and starting the {@link TeensyService}
	 * 
	 * @param firmware			The firmware ID number
	 * @param label				The label to display connection status. May be <b>null</b>.
	 * @param shape				The shape to display connection status. May be <b>null</b>.
	 */
	private void startService(final int firmware) {
		firmwareId = firmware;
		
		connectionTimer = new Timer();
		connectionTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (getState() == EConnectionState.STATE_DISCONNECTED) {
                	tryConnect();
                } else if (getState() == EConnectionState.STATE_CONNECTED) {
                	try {
						checkConnection();
					} catch (TeensyServiceException e) {
						updateState(EConnectionState.STATE_DISCONNECTED);
					}
                }
            }
        }, 0, CONNECT_REPEAT_TIME);
	}
	
	/**
	 * Stop communication with a teensy and free resources
	 * 
	 * @throws TeensyServiceException 
	 */
	public synchronized void stopService() {
		try {
			updateState(EConnectionState.STATE_DISCONNECTING);
			
			if (connectionTimer != null) {
				connectionTimer.cancel();
				connectionTimer = null;
			}
			
			if (serialPort != null) {
				if (serialPort.isOpen()) {
					serialPort.closePort();
				}
				serialPort = null;
			}
			
			updateState(EConnectionState.STATE_DISCONNECTED);
		} catch (Exception e) {
			updateState(EConnectionState.STATE_DISCONNECTED);
		}
	}
	
	/**
	 * Check connection and try estabilish connection, if disconnected
	 */
	private synchronized void tryConnect() {
		if (connectionState == EConnectionState.STATE_DISCONNECTED) {
			updateState(EConnectionState.STATE_CONNECTING);
			
			 try {
				 final SerialPort [] ports = SerialPort.getCommPorts();
				 final TeensyRequest id_packet = new TeensyRequest(TeensyConst.CMD_GET_ID);
				
				 for (final SerialPort port: ports) {
					 serialPort = port;
					 
					 serialPort.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING | SerialPort.TIMEOUT_READ_BLOCKING, 1000, 1000);
					 serialPort.setBaudRate(BAUD_RATE);

					 try {
						 serialPort.openPort();

						 final TeensyResponse response = sendPacket(id_packet, false);
						 
						 if (response.readByte() == firmwareId) {
							 updateState(EConnectionState.STATE_CONNECTED);
							 return;
						 } else {
							 serialPort.closePort();
							 serialPort = null;
						 }
					 } catch (Exception e) {
						 if (serialPort.isOpen()) {
							 serialPort.closePort();
						 }
						 
						 serialPort = null;
					 }
				 }
				 
				 updateState(EConnectionState.STATE_DISCONNECTED);
			 } catch (Exception e) {
				 updateState(EConnectionState.STATE_DISCONNECTED);
				 serialPort = null;
			 }
		}
	}
	
	/**
	 * Send a command to the teensy baord and returns the answer
	 * 
	 * @param packet		The {@link TeensyRequest}
	 * 
	 * @return				null or readed array
	 * 
	 * @throws TeensyServiceException 
	 */
	public synchronized TeensyResponse sendPacket(final TeensyRequest packet, final boolean waitAck) throws TeensyServiceException {
		try {
			if (connectionState == EConnectionState.STATE_CONNECTED || connectionState == EConnectionState.STATE_CONNECTING) {
				
				byte [] ack = new byte [1];
				do {
					if (serialPort.bytesAvailable() > 0) {
						final byte [] temp_buf = new byte [serialPort.bytesAvailable()];
						serialPort.readBytes(temp_buf, temp_buf.length);
						
						ack[0] = TeensyConst.ACK_OK;
						serialPort.writeBytes(ack, 1);											// Send ACK_OK for residual packet
					}

					serialPort.writeBytes(packet.getPacket(), packet.getPacketLength());
					serialPort.readBytes(ack, 1);
					if (ack[0] == TeensyConst.START_FRAME) {
						ack[0] = TeensyConst.ACK_OK;
						serialPort.writeBytes(ack, 1);
					}
				} while (ack[0] != TeensyConst.ACK_OK && waitAck);

				if (ack[0] == TeensyConst.ACK_OK) {
					return readResponse();
				} else {
					return null;
				}
			}
		} catch (Exception e) {
			updateState(EConnectionState.STATE_DISCONNECTED);
		}
		
		return null;
	}
	
	/**
	 * Read response from serial
	 * 
	 * @return	The {@link TeensyResponse} instance or null if error
	 */
	private TeensyResponse readResponse() {
		TeensyResponse result = null;
		byte command, commandResult;
		byte [] data = null;
		
		try {
			long checksum = 0, checksum_header = 0;
			byte [] response = new byte [1];
		
			do {
				do {
					serialPort.readBytes(response, 1);
				} while (response[0] != TeensyConst.START_FRAME);

				// Read header
				final byte [] header = new byte[TeensyConst.HEADER_LENGTH];
		
				serialPort.readBytes(header, header.length);
		
				command = header[0];
				commandResult = header[1];
		
				if (header[2] != 0) {
					data = new byte[header[2]];
				}
		
				checksum_header = header[3] + (header[4] << 8) + (header[5] << 16) + (header[6] << 24);
		
				if (data != null && data.length > 0) {
					serialPort.readBytes(data, data.length);
					checksum = TeensyService.adler32(data);
				}

				if (checksum != checksum_header) {
					response[0] = TeensyConst.ACK_FAIL;
					serialPort.writeBytes(response, 1);
				} else {
					response[0] = TeensyConst.ACK_OK;
					serialPort.writeBytes(response, 1);
				}
			} while (checksum_header != checksum);
			
			result = new TeensyResponse(command, commandResult, data);
		} catch (Exception e) {
			
		}
		
		return result;
	}
	
	/**
	 * Request ID from teensy, and update state to = STATE_DISCONNECTED if no answer
	 * 
	 * @throws TeensyServiceException 
	 */
	private void checkConnection() throws TeensyServiceException {
		if (connectionState == EConnectionState.STATE_CONNECTED) {
			try {
				final TeensyResponse response = sendPacket(new TeensyRequest(TeensyConst.CMD_NOP), true);
				
				if (response.getResult() != 0) {
					throw new TeensyServiceException("Error executing NOP");
				}
			} catch (Exception e) {
				updateState(EConnectionState.STATE_DISCONNECTED);
			}
		}
	}
		
	/**
	 * Update connection status and notify all listeners
	 * 
	 * @param newState		The {@link EConnectionState} to notify
	 * 
	 * @throws TeensyServiceException 
	 */
	private void updateState(final EConnectionState newState) {
		connectionState = newState;
		
		try {
			switch (connectionState) {
			case STATE_CONNECTED:
				notify("onTeensyConnected");
				break;
			case STATE_CONNECTING:
				notify("onTeensyConnecting");
				break;
			case STATE_DISCONNECTED:
				notify("onTeensyDisconnected");
				break;
			case STATE_DISCONNECTING:
				notify("onTeensyDisconnecting");
				break;
			}
		} catch (Exception e) {
		}
	}
	
	/**
	 * Calculate Adler-32 checksum
	 * 
	 * @param data	The byte array
	 * 
	 * @return		Adler-32 checksum
	 */
	public static long adler32(byte [] data) {
		int a = 1, b = 0;
		
		for (int i = 0; i < data.length; i++) {
			a += (int)(data[i] & 0xFF) % 65521;
			b += a % 65521;
		}
		
		return ((b & 0xFFFF) << 16) + (a & 0xFFFF);
	}
}

package de.fh_zwickau.teensy;

/**
 * The teensy event listener
 * 
 * @author Alexandr Mitiaev
 *
 */
public interface TeensyServiceListener {
	/**
	 * Start connecting to the teensy
	 */
	public void onTeensyConnecting();
	
	/**
	 * Disconnected from the teensy
	 */
	public void onTeensyConnected();
	
	/**
	 * Connected to the teensy
	 */
	public void onTeensyDisconnected();
	
	/**
	 * Disconnect from the teensy started
	 */
	public void onTeensyDisconnecting();
}

package de.fh_zwickau.teensy;

/**
 * Constants for the teensy service
 * 
 * @author Alexandr Mitiaev
 *
 */
public class TeensyConst {
	/**
	 * The length of the packet header
	 */
	public static final int HEADER_LENGTH					=					7;
	
	/**
	 * Acknowledgement flag: packet receive fail
	 */
	public static final byte ACK_FAIL						=					(byte)0x00;
	
	/**
	 * Acknowledgement flag: packet receive ok
	 */
	public static final byte ACK_OK							=					(byte)0x01;
	
	/**
	 * Start frame signature
	 */
	public static final byte START_FRAME					=					(byte) 0xAA;
	
	/**
	 * "Get ID" command code on the teensy board
	 */
	public static final byte CMD_GET_ID						=					(byte) 0xFF;
	
	/**
	 * "Nop" command code on the teensy board (to check connection state)
	 */
	public static final byte CMD_NOP						=					(byte) 0xFE;
	
	/**
	 * The size of <code>byte</code> datatype in bytes
	 */
	public static final int SIZE_OF_BYTE					=					Byte.BYTES;
	
	/**
	 * The size of <code>short</code> datatype in bytes
	 */
	public static final int SIZE_OF_SHORT					=					Short.BYTES;
	
	/**
	 * The size of <code>int</code> datatype in bytes
	 */
	public static final int SIZE_OF_INT						=					Integer.BYTES;
	
	/**
	 * The size of <code>long</code> datatype in bytes
	 */
	public static final int SIZE_OF_LONG					=					Long.BYTES;
	
	/**
	 * The size of <code>double</code> datatype in bytes
	 */
	public static final int SIZE_OF_DOUBLE					=					Double.BYTES;
}

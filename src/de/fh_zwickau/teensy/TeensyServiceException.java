package de.fh_zwickau.teensy;

/**
 * Exception class for the {@link TeensyService}
 * 
 * @author Alexandr Mitiaev
 *
 */
public class TeensyServiceException extends Exception {
	/**
	 * Generated UID
	 */
	private static final long serialVersionUID = -4092342062442484078L;

	/**
	 * Create new {@link TeensyServiceException} instance
	 */
	public TeensyServiceException() {
		super();
	}
	
	/**
	 * Create new {@link TeensyServiceException} instance with a exception-message
	 * 
	 * @param message	The exception message
	 */
	public TeensyServiceException(final String message) {
		super(message);
	}
	
	/**
	 * Create new {@link TeensyServiceException} instance based on another {@link Exception}
	 * 
	 * @param ex	The {@link Exception} instance
	 */
	public TeensyServiceException(final Exception ex) {
		super(ex);
	}
}

package de.fh_zwickau.teensy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Generic notifyer class
 * 
 * @author Alexandr Mitiaev
 * 
 */
public class GenericNotifyer<T> {
	/**
	 * List of the listeners
	 */
	protected final ArrayList<T> listeners = new ArrayList<T>();

	/**
	 * Add listener
	 * 
	 * @param listener The {@link T} to adding
	 */
	public synchronized void addListener(final T listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}
	
	/**
	 * Remove listener
	 * 
	 * @param listener The listener to remove
	 */
	public synchronized void removeListener(final T listener) {
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}
	
	/**
	 * Return list of a registered listeners
	 * 
	 * @return list of the {@link T}
	 */
	protected synchronized final ArrayList<T> getListeners() {
		return this.listeners;
	}
	
	/**
	 * Notify listeners via calling callback method <code>methodName</code>
	 * 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	protected synchronized void notify(final String methodName, Object ... args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		for (final T listener: listeners) {
			final Class<?> params[] = new Class[args.length];
			
			for (int i = 0; i < args.length; i++) {
				params[i] = args[i].getClass();
			}
			
			final Method method = listener.getClass().getMethod(methodName, params);
			method.invoke(listener, args);
		}
	}
}

package de.fh_zwickau.teensy;

import java.nio.ByteBuffer;
import com.fazecast.jSerialComm.SerialPort;

/**
 * The class to represent a data packet for a teensy board
 * 
 * @author Alexandr Mitiaev
 *
 */
public class TeensyResponse {
	/**
	 * Data to send
	 */
	private byte [] data									=					null;
	
	/**
	 * Current reading position in the buffer
	 */
	private byte currentPosition							=					0;
	
	/**
	 * The command code
	 */
	private byte command;
	
	/**
	 * Command result: 0 if success, otherwise error code
	 */
	private byte commandResult;
	
	/**
	 * Create new {@link TeensyResponse}
	 * 
	 * @param serialPort		The {@link SerialPort} instance to read response
	 * 
	 * @throws TeensyServiceException 
	 */
	public TeensyResponse(final byte command, final byte commandResult, final byte [] data) throws TeensyServiceException {
		this.command = command;
		this.commandResult = commandResult;
		this.data = data;
	}
	
	/**
	 * Returns the command code
	 * 
	 * @return	The command code
	 */
	public byte getCommand() {
		return command;
	}
	
	/**
	 * Returns the command result
	 * 
	 * @return	0 if success, otherwise error code
	 */
	public byte getResult() {
		return commandResult;
	}
	
	/**
	 * Reset the reading pointer of the buffer
	 */
	public void resetBuffer() {
		currentPosition = 0;
	}
	
	/**
	 * Read a single byte value from response data
	 * 
	 * @throws TeensyServiceException 
	 */
	public byte readByte() throws TeensyServiceException {
		if (currentPosition <= data.length - TeensyConst.SIZE_OF_BYTE) {
			return data[currentPosition++];
		} else {
			throw new TeensyServiceException("Error getting value: end of packet reached");
		}
	}
	
	/**
	 * Read a single short value from response data
	 * 
	 * @throws TeensyServiceException 
	 */
	public short readShort() throws TeensyServiceException {
		if (currentPosition <= data.length - TeensyConst.SIZE_OF_SHORT) {
			short result = 0;
			for (int i = 0; i < TeensyConst.SIZE_OF_SHORT; i++) {
				result += (short)(data[currentPosition++] << (8 * i));
			}
			
			return result;
		} else {
			throw new TeensyServiceException("Error getting value: end of packet reached");
		}
	}
	
	/**
	 * Read a single integer value from response data
	 * 
	 * @throws TeensyServiceException 
	 */
	public int readInt() throws TeensyServiceException {
		if (currentPosition <= data.length - TeensyConst.SIZE_OF_INT) {
			int result = 0;
			for (int i = 0; i < TeensyConst.SIZE_OF_INT; i++) {
				result += (int)(data[currentPosition++] << (8 * i));
			}
			
			return result;
		} else {
			throw new TeensyServiceException("Error getting value: end of packet reached");
		}
	}
	
	/**
	 * Read a single long value from response data
	 * 
	 * @throws TeensyServiceException 
	 */
	public long readLong() throws TeensyServiceException {
		if (currentPosition <= data.length - TeensyConst.SIZE_OF_LONG) {
			long result = 0;
			for (int i = 0; i < TeensyConst.SIZE_OF_LONG; i++) {
				result += (long)(data[currentPosition++] << (8 * i));
			}
			
			return result;
		} else {
			throw new TeensyServiceException("Error getting value: end of packet reached");
		}
	}
	
	/**
	 * Read a single long value from response data
	 * 
	 * @throws TeensyServiceException 
	 */
	public double readDouble() throws TeensyServiceException {
		if (currentPosition <= data.length - TeensyConst.SIZE_OF_DOUBLE) {
			final byte [] double_buffer = new byte [TeensyConst.SIZE_OF_DOUBLE];
			System.arraycopy(data, currentPosition, double_buffer, 0, TeensyConst.SIZE_OF_DOUBLE);
			
			currentPosition += TeensyConst.SIZE_OF_DOUBLE;
			
			return ByteBuffer.wrap(double_buffer).getDouble();
		} else {
			throw new TeensyServiceException("Error getting value: end of packet reached");
		}
	}
	
	/**
	 * Read a null-terminated string from response data
	 * 
	 * @param value		The string-value to adding
	 */
	public String readString() {
		final StringBuilder sb = new StringBuilder();
		
		while (currentPosition < data.length) {
			if (data[currentPosition] != 0x00) {
				sb.append((char)data[currentPosition++]);
			} else {
				break;
			}
		}
		
		return sb.toString();
	}

	/**
	 * Returns size of the data-block of the packet
	 * 
	 * @return	The data size
	 */
	public int getDataLength() {
		if (data == null) {
			return 0;
		} else {
			return data.length;
		}
	}
}

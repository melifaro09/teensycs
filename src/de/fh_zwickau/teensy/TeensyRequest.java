package de.fh_zwickau.teensy;

import java.nio.ByteBuffer;

/**
 * The class to represent a data packet for a teensy board
 * 
 * @author Alexandr Mitiaev
 *
 */
public class TeensyRequest {
	/**
	 * The command code
	 */
	private byte command;
	
	/**
	 * Data to send
	 */
	private byte [] data					=			null;
	
	/**
	 * Create new {@link TeensyRequest}
	 * 
	 * @param command		The command code
	 */
	public TeensyRequest(final byte command) {
		this.command = command;
	}
	
	/**
	 * Increase {@link #data} array size
	 * 
	 * @param size	The size to add
	 */
	private void expandData(final int size) {
		if (data == null) {
			data = new byte [size];
		} else {
			byte [] new_data = new byte [data.length + size];
			System.arraycopy(data, 0, new_data, 0, data.length);
			data = new_data;
		}
	}
	
	/**
	 * Append byte-variable to the data packet
	 * 
	 * @param value		The byte-value to adding
	 */
	public void writeByte(final byte value) {
		expandData(TeensyConst.SIZE_OF_BYTE);
		data[data.length - 1] = value;
	}
	
	/**
	 * Append short-variable to the data packet
	 * 
	 * @param value		The short-value to adding
	 */
	public void writeShort(final short value) {
		expandData(TeensyConst.SIZE_OF_SHORT);
		
		for (int i = 0; i < TeensyConst.SIZE_OF_SHORT; i++) {
			data[data.length - TeensyConst.SIZE_OF_SHORT + i] = (byte)((value >> (8 * i)) & 0xFF);
		}
	}
	
	/**
	 * Append int-variable to the data packet
	 * 
	 * @param value		The int-value to adding
	 */
	public void writeInt(final int value) {
		expandData(TeensyConst.SIZE_OF_INT);
		
		for (int i = 0; i < TeensyConst.SIZE_OF_INT; i++) {
			data[data.length - TeensyConst.SIZE_OF_INT + i] = (byte)((value >> (8 * i)) & 0xFF);
		}
	}
	
	/**
	 * Append long-variable to the data packet
	 * 
	 * @param value		The long-value to adding
	 */
	public void writeLong(final long value) {
		expandData(TeensyConst.SIZE_OF_LONG);
		
		for (int i = 0; i < TeensyConst.SIZE_OF_LONG; i++) {
			data[data.length - TeensyConst.SIZE_OF_LONG + i] = (byte)((value >> (8 * i)) & 0xFF);
		}
	}
	
	/**
	 * Append double-variable to the data packet
	 * 
	 * @param value		The double-value to adding
	 */
	public void writeDouble(final double value) {
		expandData(TeensyConst.SIZE_OF_DOUBLE);
		
		byte [] bytes = new byte[TeensyConst.SIZE_OF_DOUBLE];
		ByteBuffer.wrap(bytes).putDouble(value);
		
		System.arraycopy(bytes, 0, data, data.length - TeensyConst.SIZE_OF_DOUBLE, TeensyConst.SIZE_OF_DOUBLE);
	}
	
	/**
	 * Append string-variable to the data packet
	 * 
	 * @param value		The string-value to adding
	 */
	public void writeString(final String value) {
		expandData(value.length() + 1);
		
		for (int i = 0; i < value.length(); i++) {
			data[data.length - value.length() + i - 1] = (byte) value.charAt(i);
		}
		
		data[data.length - 1] = 0x00;				// Final 0x00 byte for a string
	}
	
	/**
	 * Build the data packet and return it as a byte array
	 * 
	 * @return	The data packet as a byte array
	 */
	public byte [] getPacket() {
		byte [] packet = null;
		long checksum = 0;
		
		if (data != null) {
			checksum = TeensyService.adler32(data);
			packet = new byte [data.length + TeensyConst.HEADER_LENGTH];
		} else {
			packet = new byte [TeensyConst.HEADER_LENGTH];
		}
		
		packet[0] = TeensyConst.START_FRAME;				// Start frame 0xAA
		packet[1] = command;								// Command code
		
		if (data != null) {									// Data size
			packet[2] = (byte) data.length;
		} else {
			packet[2] = (byte) 0;
		}
		
		packet[3] = (byte)(checksum & 0xFF);				// CRC32 of the data block
		packet[4] = (byte)((checksum >> 8) & 0xFF);
		packet[5] = (byte)((checksum >> 16) & 0xFF);
		packet[6] = (byte)((checksum >> 24) & 0xFF);
		
		if (data != null) {
			System.arraycopy(data, 0, packet, TeensyConst.HEADER_LENGTH, data.length);
		}
		
		return packet;
	}
	
	/**
	 * Returns size of the data-block of the packet
	 * 
	 * @return	The data size
	 */
	public int getDataLength() {
		if (data == null) {
			return 0;
		} else {
			return data.length;
		}
	}
	
	/**
	 * Returns size of a packet
	 * 
	 * @return	Size of a packet
	 */
	public int getPacketLength() {
		return getDataLength() + TeensyConst.HEADER_LENGTH;
	}
}

#include <util/delay.h>

#define   FIRMWARE_ID       0x09
#define   SERIAL_SPEED      115200

#define   CMD_GET_ID        0xFF
#define   CMD_NOP           0xFE

#define   CMD_TOGGLE        0x01

#define   ACK_FAIL          0x00
#define   ACK_OK            0x01

#define   START_FRAME       0xAA

#define   PIN_OE            17      // "Output Enable" pin number
#define   PIN_LE            14
#define   PIN_CLK           15
#define   PIN_SDI           16

//spi_t* spi;

/**
 * Serial port header structure
 */
struct t_serial_header {
  uint8_t   cmd;            // Command code
  uint8_t   data_len;       // Data length
  uint32_t  checksum;       // Checksum of the data
} request_header;

/**
 * Set up
 */
void setup() {
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  pinMode(PIN_OE, OUTPUT);        // Enable OE (active low)
  digitalWrite(PIN_OE, LOW);

  pinMode(PIN_LE, OUTPUT);        // Disable LE
  digitalWrite(PIN_LE, LOW);

  pinMode(PIN_SDI, OUTPUT);
  digitalWrite(PIN_SDI, LOW);

  pinMode(PIN_CLK, OUTPUT);
  digitalWrite(PIN_CLK, LOW);
  
  //spi = spi_init(0, 16, 15, 0, 0, SPI_MODE1);
  Serial.begin(SERIAL_SPEED);
}

/**
 * Wait for 'data_size' bytes on serial
 */
void serial_wait(uint8_t data_size) {
  while (Serial.available() < data_size) ;
}

uint8_t serial_read_uint8() {
  serial_wait(1);
  return Serial.read();
}

uint16_t serial_read_uint16() {
  serial_wait(2);
  return Serial.read() + (Serial.read() << 8);
}

uint32_t serial_read_uint32() {
  serial_wait(4);
  return Serial.read() + (Serial.read() << 8) + (Serial.read() << 16) + (Serial.read() << 24);
}

void serial_write_uint8(uint8_t value) {
  Serial.write(value);
}

void serial_write_uint16(uint16_t value) {
  Serial.write(value & 0xFF);
  Serial.write((value >> 8) & 0xFF);
}

void serial_write_uint32(uint32_t value) {
  Serial.write(value & 0xFF);
  Serial.write((value >> 8) & 0xFF);
  Serial.write((value >> 16) & 0xFF);
  Serial.write((value >> 24) & 0xFF);
}

/**
 * Calculate Adler-32 checksum
 */
uint32_t adler32(uint8_t* data, uint8_t len) {
  uint32_t a = 1, b = 0;
  
  for (int i = 0; i < len; i++) {
    a += data[i] % 65521;
    b += a % 65521;
  }

  return ((b & 0xFFFF) << 16) + (a & 0xFFFF);
}

/**
 * Build response and send it over serial
 */
void sendResponse(uint8_t cmd, uint8_t cmd_result, uint8_t* data, uint8_t data_len) {
  do {
    Serial.write(START_FRAME);      // Start frame
    Serial.write(cmd);              // Command code
    Serial.write(cmd_result);       // Command result: 0 - success, otherwise error code
    Serial.write(data_len);         // Data length

    uint32_t checksum = 0;
    if (data_len > 0 && data != NULL) {
      checksum = adler32(data, data_len);
    }
    serial_write_uint32(checksum);

    for (int i = 0; i < data_len; i++) {
      Serial.write(data[i]);
    }
    serial_wait(1);
  } while (Serial.read() != 1);   // Wait ACK

  if (data != NULL) {
    delete [] data;
  }
}

/**
 * Parse received command
 */
void parseCommand(const t_serial_header& header, uint8_t* data) {
  uint8_t* response;
  uint16_t data16;
  uint32_t data32;

  switch (header.cmd) {
    case CMD_GET_ID:
      response = new uint8_t[1];
      response[0] = FIRMWARE_ID;
        sendResponse(CMD_GET_ID, 0, response, 1);
        break;
      
      case CMD_NOP:
        sendResponse(CMD_NOP, 0, NULL, 0);
        break;

      case CMD_TOGGLE:
        if (data[0] == 0xFF) {
          digitalWrite(13, HIGH);
        } else {
          digitalWrite(13, LOW);
        }

        digitalWrite(PIN_OE, HIGH);         // Disable outputs
        delay(5);
        
        data16 = data[0] + (data[1] << 8);
        for (int i = 0; i < 16; i++) {
          digitalWrite(PIN_SDI, (data16 & (0x01 << i)) >> i);
          delay(5);
          digitalWrite(PIN_CLK, HIGH);
          delay(5);
          digitalWrite(PIN_SDI, LOW);
          delay(5);
          digitalWrite(PIN_CLK, LOW);
          delay(5);
        }

        digitalWrite(PIN_CLK, HIGH);
        digitalWrite(PIN_LE, HIGH);
        delay(10);

        digitalWrite(PIN_CLK, LOW);
        digitalWrite(PIN_LE, LOW);
        delay(10);

        digitalWrite(PIN_SDI, LOW);
        digitalWrite(PIN_OE, LOW);

        sendResponse(CMD_TOGGLE, 0, NULL, 0);
        break;
    }

  if (data != NULL) {
    delete [] data;
  }
}

/**
 * Main loop
 */
void loop() {
  if (Serial.available()) {
    while (Serial.read() != START_FRAME) ;           // Wait for start frame

    request_header.cmd         = serial_read_uint8();
    request_header.data_len    = serial_read_uint8();
    request_header.checksum    = serial_read_uint32();

    uint32_t checksum = 0;
    uint8_t* data_buffer = NULL;
    
    if (request_header.data_len > 0) {
      data_buffer = new uint8_t[request_header.data_len];

      serial_wait(request_header.data_len);
      for (int i = 0; i < request_header.data_len; i++) {
        data_buffer[i] = Serial.read();
      }
      checksum = adler32(data_buffer, request_header.data_len);
    }

    if (checksum != request_header.checksum) {
      Serial.write(ACK_FAIL);
    } else {
      Serial.write(ACK_OK);
      parseCommand(request_header, data_buffer);
    }
  }
}


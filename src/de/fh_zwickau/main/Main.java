package de.fh_zwickau.main;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.prefs.Preferences;

import de.fh_zwickau.main.controllers.MainViewController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

/**
 * Main class
 * 
 * @author Alexandr Mitiaev
 *
 */
public class Main extends Application {
	/**
	 * The primary stage
	 */
	private static Stage primaryStage = null;
	
	/**
	 * The {@link Preferences} to store common app settings
	 */
	private static Preferences prefs;
	
	/**
	 * Start app
	 */
	@Override
	public void start(Stage pStage) {
		try {
			prefs = Preferences.userNodeForPackage(getClass());
			
			primaryStage = pStage;
			
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("views/MainView.fxml"));
			Scene scene = new Scene(root);
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("TeensyService tester");
			primaryStage.show();
			
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			      public void handle(WindowEvent we) {
			          MainViewController.closeApp();
			      }
			  }); 
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Display alert dialog with a message
	 * 
	 * @param alertType
	 *            Type of alert (Error, Information, Confirmation, Warning)
	 * @param title
	 *            Title of the dialog
	 * @param header
	 *            Header of the dialog
	 * @param content
	 *            Message text
	 */
	private static void alert(final AlertType alertType, final String title, final String header, final String content) {
		final Alert alert = new Alert(alertType);

		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.show();		
	}
	
	/**
	 * Display alert dialog with a message
	 * 
	 * @param alertType
	 *            Type of alert (Error, Information, Confirmation, Warning)
	 * @param title
	 *            Title of the dialog
	 * @param header
	 *            Header of the dialog
	 * @param content
	 *            Message text
	 */
	public static void showAlert(final AlertType alertType, final String title, final String header, final String content) {
		if (Platform.isFxApplicationThread()) {
			alert(alertType, title, header, content);
		} else {
			Platform.runLater(new Runnable() {
				public void run() {
					alert(alertType, title, header, content);
				}
			});
		}
	}

	/**
	 * Show confirmation dialog. Returns true, if "Yes" clicked, otherwise false
	 * 
	 * @param title
	 *            Title of the dialog
	 * @param header
	 *            Header of the dialog
	 * @param message
	 *            Message text
	 * 
	 * @return true, if "Yes" clicked, otherwise false
	 */
	private static boolean confirmDialog(String title, String header, String message) {
		final Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(message);
		alert.getButtonTypes().clear();
		alert.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);

		if (alert.showAndWait().get() == ButtonType.YES)
			return true;

		return false;
	}
	
	/**
	 * Show confirmation dialog. Returns true, if "Yes" clicked, otherwise false
	 * 
	 * @param title
	 *            Title of the dialog
	 * @param header
	 *            Header of the dialog
	 * @param message
	 *            Message text
	 * 
	 * @return true, if "Yes" clicked, otherwise false
	 */
	public static boolean showConfirmDialog(final String title, final String header, final String message) {
		if (Platform.isFxApplicationThread()) {
			return confirmDialog(title, header, message);
		} else {
			final FutureTask<Boolean> query = new FutureTask<Boolean>(new Callable<Boolean>() {
				public Boolean call() throws Exception {
					return confirmDialog(title, header, message);
				}
			});
			
			Platform.runLater(query);
			
			try {
				return query.get();
			} catch (Exception e) {
				return false;
			}
		}
	}
	
	/**
	 * Select folder and return path as a string
	 * 
	 * @return Selected folder or null
	 */
	private static String selectFolder(final String title, final File initialDir) {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle(title);
		
		if (initialDir != null && initialDir.exists()) {
			chooser.setInitialDirectory(initialDir);
		}

		File selectedDirectory = chooser.showDialog(primaryStage);
		
		if (selectedDirectory == null || !selectedDirectory.exists()) {
			return null;
		} else {
			return selectedDirectory.getAbsolutePath();
		}
	}
	
	/**
	 * Select folder and return path as a string
	 *
	 * @param title			Dialog title
	 * @param initialDir	Initial directory
	 * 
	 * @return Selected folder or null
	 */
	public static String showSelectFolder(final String title, final File initialDir) {
		if (Platform.isFxApplicationThread()) {
			return selectFolder(title, initialDir);
		} else {
			final FutureTask<String> query = new FutureTask<String>(new Callable<String>() {
				public String call() throws Exception {
					return selectFolder(title, initialDir);
				}
			});
			
			Platform.runLater(query);
			
			try {
				return query.get();
			} catch (Exception e) {
				return null;
			}
		}
	}
	
	/**
	 * Show open file dialog
	 * 
	 * @param title The open dialog window title
	 * 
	 * @return Selected file or null, if canceled
	 */
	private static File openFileDialog(final String title, final File initialDir, final Collection<ExtensionFilter> filters) {
		final FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(title);
		
		if (initialDir != null) {
			fileChooser.setInitialDirectory(initialDir);
		}
		
		if (filters != null) {
			fileChooser.getExtensionFilters().addAll(filters);
		}
		
		return fileChooser.showOpenDialog(primaryStage);
	}
	
	/**
	 * Show open file dialog
	 * 
	 * @param title The open dialog window title
	 * 
	 * @return Selected file or null, if canceled
	 */
	public static File showOpenFileDialog(final String title, final File initialDir, final Collection<ExtensionFilter> filters) {
		if (Platform.isFxApplicationThread()) {
			return openFileDialog(title, initialDir, filters);
		} else {
			final FutureTask<File> query = new FutureTask<File>(new Callable<File>() {
				public File call() throws Exception {
					return openFileDialog(title, initialDir, filters);
				}
			});
			
			Platform.runLater(query);
			
			try {
				return query.get();
			} catch (Exception e) {
				return null;
			}
		}
	}
	
	/**
	 * Returns the primary stage
	 * 
	 * @return The primary stage
	 */
	public static Stage getPrimaryStage() {
		return primaryStage;
	}
	
	/**
	 * Returns the {@link #prefs}
	 * 
	 * @return The {@link #prefs}
	 */
	public static Preferences getPrefs() {
		return prefs;
	}
	
	/**
	 * Launch app
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}

package de.fh_zwickau.main.controllers;

import de.fh_zwickau.teensy.TeensyService;
import de.fh_zwickau.teensy.TeensyServiceListener;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

/**
 * Main.fxml-controller
 * 
 * @author Alexandr Mitiaev
 */
public class MainViewController implements TeensyServiceListener {
	/**
	 * The teensy firmware ID
	 */
	public static final byte FIRMWARE_ID				=				(byte) 0x09;
	
	/**
	 * Color for "connected" state
	 */
	private static final String COLOR_CONNECTED			=				"green";
	
	/**
	 * Color for "connecting" state
	 */
	private static final String COLOR_CONNECTING		=				"orange";
	
	/**
	 * Color for "disconnected" state
	 */
	private static final String COLOR_DISCONNECTED		=				"red";
	
	/**
	 * Connection shape
	 */
	@FXML
	public Circle shConnectionShape;
	
	/**
	 * Connection status
	 */
	@FXML
	public Label lbConnectionStatus;
	
	/**
	 * The {@link TeensyService} to use
	 */
	private static TeensyService teensyService;
	
	/**
	 * Close application event
	 */
	public static void closeApp() {
		teensyService.stopService();
	}
	
	/**
	 * Initialize APT library
	 */
	@FXML
	public void initialize() {
		teensyService = TeensyService.buildService(FIRMWARE_ID);
		teensyService.addListener(this);
	}
	
	/**
	 * Connected to the teensy: enable visual controls and request actual LED state
	 */
	public void onTeensyConnected() {
		Platform.runLater(new Runnable() {
			public void run() {
				lbConnectionStatus.setText("Connected");
				lbConnectionStatus.setTextFill(Paint.valueOf(COLOR_CONNECTED));
				shConnectionShape.setFill(Paint.valueOf(COLOR_CONNECTED));	
			}
		});
	}

	/**
	 * Disconnected from the teensy: disable visual controls
	 */
	public void onTeensyDisconnected() {
		Platform.runLater(new Runnable() {
			public void run() {
				lbConnectionStatus.setText("Disconnected");
				lbConnectionStatus.setTextFill(Paint.valueOf(COLOR_DISCONNECTED));
				shConnectionShape.setFill(Paint.valueOf(COLOR_DISCONNECTED));	
			}
		});
	}

	/**
	 * Implementation of {@link TeensyServiceListener#onTeensyConnecting()}
	 */
	public void onTeensyConnecting() {
		Platform.runLater(new Runnable() {
			public void run() {
				lbConnectionStatus.setText("Try to connect...");
				lbConnectionStatus.setTextFill(Paint.valueOf(COLOR_CONNECTING));
				shConnectionShape.setFill(Paint.valueOf(COLOR_CONNECTING));				
			}
		});
	}

	/**
	 * Implementation of {@link TeensyServiceListener#onTeensyDisconnecting()}
	 */
	public void onTeensyDisconnecting() {
	}
}

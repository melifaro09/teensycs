package de.fh_zwickau.teensy;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import org.junit.Before;
import org.junit.Test;

/**
 * Test cases for the {@link TeensyRequest} class
 * 
 * @author Alexandr Mitiaev
 *
 */
public class TeensyRequestTest {
	/**
	 * Default command code for testing
	 */
	private static final byte DEFAULT_CMD_CODE				=			0x01;
	
	/**
	 * Length of the header in bytes
	 */
	private static final byte HEADER_LENGTH					=			7;
	
	/**
	 * Offset of the start frame in the header
	 */
	private static final int OFFSET_START_FRAME				=			0x00;
	
	/**
	 * Offset of the command code in the header
	 */
	private static final int OFFSET_COMMAND					=			0x01;
	
	/**
	 * Offset of the data length in the header
	 */
	private static final int OFFSET_DATA_LENGTH				=			0x02;
	
	/**
	 * Offset of the CRC32 checksum in the header
	 */
	private static final int OFFSET_CRC						=			0x03;
	
	/**
	 * Test byte value
	 */
	private static final byte TEST_BYTE						=			(byte)0xCE;
	
	/**
	 * Test short value
	 */
	private static final short TEST_SHORT					=			(short)0xCFFA;
	
	/**
	 * Test int value
	 */
	private static final int TEST_INT						=			(int)0xABCD9876;
	
	/**
	 * Test long value
	 */
	private static final long TEST_LONG						=			(long)0x01020304;
	
	/**
	 * Test string value
	 */
	private static final String TEST_STRING					=			"Hallo world!";
	
	/**
	 * Test double value
	 */
	private static final double TEST_DOUBLE					=			(double)123.456;
	
	/**
	 * The class under test
	 */
	private TeensyRequest classUnderTest;

	/**
	 * Create instance of the {@link TeensyRequest}
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		classUnderTest = new TeensyRequest(DEFAULT_CMD_CODE);
	}
	
	/**
	 * Testing {@link TeensyRequest#getDataLength()} with a empty data
	 */
	@Test
	public void testEmptyDataLength() {
		assertEquals(0, classUnderTest.getDataLength());
	}
	
	/**
	 * Testing {@link TeensyRequest#getPacketLength()} with a empty data
	 */
	@Test
	public void testEmptyPacketLength() {
		assertEquals(HEADER_LENGTH, classUnderTest.getPacketLength());
	}
	
	/**
	 * Testing {@link TeensyRequest#writeByte(byte)} method
	 */
	@Test
	public void testWriteByte() {
		classUnderTest.writeByte(TEST_BYTE);
		assertEquals(Byte.BYTES, classUnderTest.getDataLength());
		
		assertEquals(TEST_BYTE, classUnderTest.getPacket()[HEADER_LENGTH]);
	}
	
	/**
	 * Testing {@link TeensyRequest#writeShort(short)} method
	 */
	@Test
	public void testWriteShort() {
		classUnderTest.writeShort(TEST_SHORT);
		assertEquals(Short.BYTES, classUnderTest.getDataLength());
		
		for (int i = 0; i < Short.BYTES; i++) {
			assertEquals((byte)((TEST_SHORT >> (i * 8)) & 0xFF), classUnderTest.getPacket()[HEADER_LENGTH + i]);
		}
	}
	
	/**
	 * Testing {@link TeensyRequest#writeInt(int)} method
	 */
	@Test
	public void testWriteInt() {
		classUnderTest.writeInt(TEST_INT);
		assertEquals(Integer.BYTES, classUnderTest.getDataLength());
		
		for (int i = 0; i < Integer.BYTES; i++) {
			assertEquals((byte)((TEST_INT >> (i * 8)) & 0xFF), classUnderTest.getPacket()[HEADER_LENGTH + i]);
		}
	}
	
	/**
	 * Testing {@link TeensyRequest#writeLong(long)} method
	 */
	@Test
	public void testWriteLong() {
		classUnderTest.writeLong(TEST_LONG);
		assertEquals(Long.BYTES, classUnderTest.getDataLength());
		
		for (int i = 0; i < Long.BYTES; i++) {
			assertEquals((byte)((TEST_LONG >> (i * 8)) & 0xFF), classUnderTest.getPacket()[HEADER_LENGTH + i]);
		}
	}
	
	/**
	 * Testing {@link TeensyRequest#writeString(String)} method
	 */
	@Test
	public void testWriteString() {
		classUnderTest.writeString(TEST_STRING);
		
		assertEquals(TEST_STRING.length() + 1, classUnderTest.getDataLength());
		
		for (int i = 0; i < TEST_STRING.length(); i++) {
			assertEquals(TEST_STRING.charAt(i), classUnderTest.getPacket()[HEADER_LENGTH + i]);
		}
	}
	
	/**
	 * Testing {@link TeensyRequest#writeDouble(double)} method
	 */
	@Test
	public void testWriteDouble() {
		classUnderTest.writeDouble(TEST_DOUBLE);
		
		assertEquals(Double.BYTES, classUnderTest.getDataLength());
		
		byte [] double_value = new byte[Double.BYTES];
		ByteBuffer.wrap(double_value).putDouble(TEST_DOUBLE);
		
		for (int i = 0; i < Double.BYTES; i++) {
			assertEquals(double_value[i], classUnderTest.getPacket()[HEADER_LENGTH + i]);
		}
	}
	
	/**
	 * Testing {@link TeensyRequest#getDataLength()} and {@link TeensyRequest#getPacketLength()} methods
	 */
	@Test
	public void testGetPacketSize() {
		assertEquals(0, classUnderTest.getDataLength());
		assertEquals(HEADER_LENGTH, classUnderTest.getPacketLength());
		
		classUnderTest.writeInt(TEST_INT);
		assertEquals(Integer.BYTES, classUnderTest.getDataLength());
		assertEquals(Integer.BYTES + HEADER_LENGTH, classUnderTest.getPacketLength());
	}
	
	/**
	 * Testing header structure, given by {@link TeensyRequest#getPacket()} method
	 */
	@Test
	public void testHeader() {
		final byte [] packet = classUnderTest.getPacket();
		
		assertEquals(TeensyConst.START_FRAME, packet[OFFSET_START_FRAME]);
		assertEquals(DEFAULT_CMD_CODE, packet[OFFSET_COMMAND]);
		assertEquals(0, packet[OFFSET_DATA_LENGTH]);
	}
	
	/**
	 * Test Adler-32 of a data in the header
	 */
	@Test
	public void testChecksum() {
		classUnderTest.writeString(TEST_STRING);
		
		final byte [] byte_str = new byte[TEST_STRING.length() + 1];
		System.arraycopy(TEST_STRING.getBytes(), 0, byte_str, 0, TEST_STRING.length());
		
		long checksum = TeensyService.adler32(byte_str);
		
		for (int i = 0; i < 4; i++) {
			assertEquals((checksum >> (i * 8)) & 0xFF, classUnderTest.getPacket()[OFFSET_CRC + i]);
		}
	}
}

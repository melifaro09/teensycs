package de.fh_zwickau.teensy;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Test cases for the {@link TeensyResponse} class
 * 
 * @author Alexandr Mitiaev
 *
 */
public class TeensyResponseTest {
	/**
	 * Command ID for a tests
	 */
	public static final byte TEST_CMD_ID			=			(byte) 0x80;
	
	/**
	 * Command result code for a tests
	 */
	public static final byte TEST_CMD_RESULT		=			(byte)0x01;
	
	/**
	 * Data for a tests
	 */
	public static final byte [] TEST_CMD_DATA		=			{ 0x31, 0x32, 0x33, 0x34, 0x00, 0x50, 0x60, 0x70 };
	
	/**
	 * The class under test
	 */
	private TeensyResponse classUnderTest;

	/**
	 * Create instance of the {@link TeensyRequest}
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		classUnderTest = new TeensyResponse(TEST_CMD_ID, TEST_CMD_RESULT, TEST_CMD_DATA);
	}
	
	/**
	 * Testing {@link TeensyResponse#getCommand()} method
	 * 
	 * @throws TeensyServiceException 
	 */
	@Test
	public void testGetCommand() throws TeensyServiceException {
		assertEquals(TEST_CMD_ID, classUnderTest.getCommand());
	}
	
	/**
	 * Testing {@link TeensyResponse#getResult()} method
	 */
	@Test
	public void testGetCommandResult() {
		assertEquals(TEST_CMD_RESULT, classUnderTest.getResult());
	}
	
	/**
	 * Testing {@link TeensyResponse#readByte()} method
	 * 
	 * @throws TeensyServiceException 
	 */
	@Test
	public void testReadByte() throws TeensyServiceException {
		for (int i = 0; i < TEST_CMD_DATA.length / Byte.BYTES; i++) {
			assertEquals(TEST_CMD_DATA[i], classUnderTest.readByte());
		}
	}
	
	/**
	 * Testing {@link TeensyResponse#readShort()} method
	 * 
	 * @throws TeensyServiceException 
	 */
	@Test
	public void testReadShort() throws TeensyServiceException {
		assertEquals(0x3231, classUnderTest.readShort());
		assertEquals(0x3433, classUnderTest.readShort());
		assertEquals(0x5000, classUnderTest.readShort());
		assertEquals(0x7060, classUnderTest.readShort());
	}
	
	/**
	 * Testing {@link TeensyResponse#readInt()} method
	 * 
	 * @throws TeensyServiceException
	 */
	@Test
	public void testReadInt() throws TeensyServiceException {
		assertEquals(0x34333231, classUnderTest.readInt());
		assertEquals(0x70605000, classUnderTest.readInt());
	}
	
	/**
	 * Testing {@link TeensyResponse#readLong()} method
	 * 
	 * @throws TeensyServiceException
	 */
	@Test
	public void testReadLong() throws TeensyServiceException {
		assertEquals(0xA4938231L, classUnderTest.readLong());
	}
	
	/**
	 * Testing {@link TeensyResponse#readDouble()} method
	 * 
	 * @throws TeensyServiceException
	 */
	@Test
	public void testReadDouble() throws TeensyServiceException {
		assertEquals(1.03E-71, classUnderTest.readDouble(), 0.01);
	}
	
	/**
	 * Testing {@link TeensyResponse#readString()} method
	 * 
	 * @throws TeensyServiceException
	 */
	@Test
	public void testReadString() throws TeensyServiceException {
		assertArrayEquals("1234".getBytes(), classUnderTest.readString().getBytes());
	}
	
	/**
	 * Testing {@link TeensyResponse#getDataLength()} method
	 */
	@Test
	public void testGetDataLength() {
		assertEquals(TEST_CMD_DATA.length, classUnderTest.getDataLength());
	}
	
	/**
	 * Testing {@link TeensyResponse#resetBuffer()} method
	 * 
	 * @throws TeensyServiceException 
	 */
	@Test
	public void testResetBuffer() throws TeensyServiceException {
		assertEquals(0x3231, classUnderTest.readShort());
		
		classUnderTest.resetBuffer();
		assertEquals(0x3231, classUnderTest.readShort());
		assertEquals(0x3433, classUnderTest.readShort());
	}
}
